1. Процесс установки и настройки ufw:

Проверяем включён ли ufw командой `ufw status`. В моём случае ufw не включён.
Настраиваем брадмауэр исходя из задачи:

```
ufw allow ssh
ufw allow https
ufw allow from 127.0.0.1 to 127.0.0.1
ufw enable
```

Проверяем конфигурацию:

```
ufw status numbered

Status: active

     To                         Action      From
     --                         ------      ----
[ 1] 22/tcp                     ALLOW IN    Anywhere
[ 2] 443/tcp                    ALLOW IN    Anywhere
[ 3] 127.0.0.1                  ALLOW IN    127.0.0.1
```

2. Процесс установки и выпуска сертификата с помощью hashicorp vault:

а) Установка:

```
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install vault
sudo apt-get install jq
```

б) Запуск:

```
vault server -dev -dev-root-token-id=root
export VAULT_ADDR=http://127.0.0.1:8200
export VAULT_TOKEN=root
```

Добавляем эти стороки

```
export VAULT_ADDR=http://127.0.0.1:8200
export VAULT_TOKEN=root
```

в файл `/etc/environment`

в) Настройка центра сертификации и выпуск сертификатов:

Создаём файл политики admin-policy.hcl

```
# Enable secrets engine
path "sys/mounts/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

# List enabled secrets engine
path "sys/mounts" {
  capabilities = [ "read", "list" ]
}

# Work with pki secrets engine
path "pki*" {
  capabilities = [ "create", "read", "update", "delete", "list", "sudo" ]
}
```

Применяем политику на сервер и проверяем:

```
vault policy write admin admin-policy.hcl
vault policy list
vault policy read admin
```

Включаем функционал pki:

```
vault secrets enable pki
```

Генерируем CA:

```
vault secrets tune -max-lease-ttl=87600h pki
vault write -field=certificate pki/root/generate/internal common_name="bychenok.ru" issuer_name="root-2022" ttl=87600h > root_2022_ca.crt
vault write pki/roles/2022-servers allow_any_name=true
vault write pki/config/urls issuing_certificates="$VAULT_ADDR/v1/pki/ca" crl_distribution_points="$VAULT_ADDR/v1/pki/crl"
```

Генерируем промежуточный CA:

```
vault secrets enable -path=pki_int pki
vault secrets tune -max-lease-ttl=43800h pki_int
vault write -format=json pki_int/intermediate/generate/internal common_name="bychenok.ru Intermediate Authority" issuer_name="root-2022" | jq -r '.data.csr' > pki_intermediate.csr
vault write -format=json pki/root/sign-intermediate issuer_ref="root-2022" csr=@pki_intermediate.csr format=pem_bundle ttl="43800h" | jq -r '.data.certificate' > intermediate.cert.pem
vault write pki_int/intermediate/set-signed certificate=@intermediate.cert.pem
```

Создаём роль:

```
vault write pki_int/roles/bychenok-dot-ru issuer_ref="$(vault read -field=default pki_int/config/issuers)" allowed_domains="bychenok.ru" allow_subdomains=true max_ttl="720h"
```

Генерируем сертификат:

```
vault write pki_int/issue/bychenok-dot-ru common_name="test.bychenok.ru" ttl="720h"
```

3. Процесс установки и настройки nginx:

a) Установка:

```
apt install nginx
```

б) Настройка:
Правим конфигурационный файл сервера по адресу `/etc/nginx/sites-available/default`. Приводим файл к следующему виду:

```
server {
	# listen 80 default_server;
	# SSL configuration
	listen 443 ssl;
	ssl_certificate		test.bychenok.ru.crt;
	ssl_certificate_key	test.bychenok.ru.key;
        ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
        ssl_ciphers         HIGH:!aNULL:!MD5;

	root /var/www/html;

	# Add index.php to the list if you are using PHP
	index index.html index.htm index.nginx-debian.html;

	server_name test.bychenok.ru;

	location / {
		# First attempt to serve request as file, then
		# as directory, then fall back to displaying a 404.
		try_files $uri $uri/ =404;
	}

}
```
Где `test.bychenok.ru.crt` - цепочка сертификатов (CA, Intermidate CA, site cert), `test.bychenok.ru.key` - приватный ключ сертификата.

4. Страница сервера nginx в браузере хоста не содержит предупреждений

![Страница браузера с сайтом](web-bro.png)

5. Скрипт генерации нового сертификата работает (сертификат сервера ngnix должен быть "зеленым")

```
#!/bin/bash
# Revoke certs
vault write pki_int/tidy safety_buffer=5s tidy_cert_store=true tidy_revocation_list=true
# Create cert, 1 month
vault write -format=json pki_int/issue/bychenok-dot-ru common_name="test.bychenok.ru" ttl="720h" > /tmp/test.bychenok.ru.pem

# save cert
cat /tmp/test.bychenok.ru.pem | jq -r .data.certificate > /etc/nginx/test.bychenok.ru.crt
cat /tmp/test.bychenok.ru.pem | jq -r .data.issuing_ca >> /etc/nginx/test.bychenok.ru.crt
cat /tmp/test.bychenok.ru.pem | jq -r .data.private_key > /etc/nginx/test.bychenok.ru.key

# nginx reload
systemctl reload nginx
```

6. Crontab работает (выберите число и время так, чтобы показать что crontab запускается и делает что надо)

Добавляем в файл планировщика `crontab -e` следующие команды:

```
50 21 7 * * /bin/bash /root/cert.sh
```

Проверяем в журнале системы `journalctl -xe`

```
Sep 07 22:05:01 bav03.test.rvision.local CRON[9014]: (root) CMD (/bin/bash /root/cert.sh)
```

Скрипт отработал. Файлы создались.